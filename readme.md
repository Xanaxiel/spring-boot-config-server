# Spring Boot Config Server

## Objectives

* Setup Spring Boot Config Server
* Pull configurations from a git repository
* Authenticate to git repository using username/password
* Protect git repository password with encryption
* Protect access to Spring Boot Config Server with basic authentication
* Store secrets in git repository encrypted
* Optionally: protect access to Spring Boot Config Server with AD/Ldap credentials

## The CloudIDE

1. This repository has been authored and tested using Cloud9 IDE as PoC for an isolated environment
2. All the dependencies have been installed and encapsulated in the setup.sh script

## Development Setup on CentOS (tested on Cloud9 IDE)

1. [Original post](http://www.baeldung.com/spring-cloud-configuration)
1. Clone the repository
1. `cd` into the repository folder
1. Run the setup script:
```
chmod +x ./setup.sh && \
./setup.sh
```

## Where are the configurations are stored?

* Configuration are stored in a git [repository](../spring-boot-config-server-settings/readme.md)
* A sample configuration file: ./customerprofile.properties in a master branch:
```
# Sample configuration settings
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.datasource.url=jdbc:mysql://localhost:3306/catalog
spring.datasource.username=dummy
spring.datasource.password={cipher}35f4bc9d7465633b929c0dbe973deabe35701497bbfdefc56663e77785e644ee361ee7437625ba8c971cb071a2b1466f
```

* Configurations can be stored in [multiple repos](https://cloud.spring.io/spring-cloud-config/single/spring-cloud-config.html#_pattern_matching_and_multiple_repositories) as well

## Where the secret is stored?

* Export environment variable: `export ENCRYPT_KEY=some-strong-secret`
* The encrypted variables can be stored in the [application.properties](./src/main/resources/application.properties) and inside the *.properties files in the settings repo

## How to encrypt the secrets?

* By POST to the /ecnrpyt end-point the string to be encrypted with basic authentication, please see above

## How the secrets are decrypted?

* Using the $ENCRYPT_KEY enviroment variables

## To run the service
```
./mvnw spring-boot:run
```

## To package the service:
```
./mvnw clean package
```

## To run the jar:
```
java -jar ./target/spring-boot-config-server-0.1.0.jar
```

## To test the end-point

* To fetch the config: `curl -v -H 'Authorization: Basic cm9vdDpzM2NyM3Q=' localhost:8888/customerprofile/master`
* To encrypt a value: `curl -v -POST -H 'Authorization: Basic cm9vdDpzM2NyM3Q=' 'http://localhost:8888/encrypt' --data-urlencode dummyStringToEncrypt`
* To encrypt/decrypt a value: 
```
encryptedValue=$(curl -s -POST -H 'Authorization: Basic cm9vdDpzM2NyM3Q=' 'http://localhost:8080/encrypt' --data-urlencode 'dummyStringToEncrypt') && \
  echo $encryptedValue && \
  curl -POST -H 'Authorization: Basic cm9vdDpzM2NyM3Q=' 'http://localhost:8888/decrypt' --data-urlencode $encryptedValue && \
  echo ""
```

## To deploy service to pcf:
```
cf login -a api.run.pivotal.io
cf push -n spring-boot-config-server -p ./target/spring-boot-config-server-0.1.0.jar   
```

## TODO:

* Actuator end-points are exposed inluding `/env` which would expose all secrets un-encrypted
* Request to fetch the credentials with proper authentication will return all encrypted values in clear text
* Disable /decrypt end-point?